# File Link Display

## Description
------------

This module provides more twig variables for the file-link.html.twig template with HOOK_preprocess_file_link(&$variables).

### This module provides:
  - Twig variables for file attributes and values.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/file_link_display

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/file_link_display


## REQUIREMENTS
------------

This module requires the following modules:

 * File


## INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
1. **Install** this module


## CONFIGURATION
------------

Available variables:

  - File Mime: {{ fileLinkDisplay.fileMime }}
  - File Size: {{ fileLinkDisplay.fileSize }}
  - File SizeFormatted: {{ fileLinkDisplay.fileSizeFormatted }}
  - File Name: {{ fileLinkDisplay.fileName }}
  - File Uri: {{ fileLinkDisplay.fileUri }}
  - File Url: {{ fileLinkDisplay.fileUrl }}
  - File Uuid: {{ fileLinkDisplay.fileUuid }}
  - File Status: {{ fileLinkDisplay.fileStatus }}
  - File CreatedTime: {{  fileLinkDisplay.fileCreatedTime | date('M, d Y') }}
  - File ChangedTime: {{  fileLinkDisplay.fileChangedTime | date('M, d Y') }}
  - File Bundle: {{ fileLinkDisplay.fileBundle }}
  - File Langcode: {{ fileLinkDisplay.fileLangcode }}
  - File Fid: {{ fileLinkDisplay.fileFid }}
  - File OwnerId: {{ fileLinkDisplay.fileOwnerId }}
  - File OwnerName: {{ fileLinkDisplay.fileOwnerName }}
  - File OwnerUserPictureUrl: {{ fileLinkDisplay.fileOwnerUserPictureUrl }}



## MAINTAINERS
-----------

Current maintainers:
 * Preston Schmidt - https://www.drupal.org/user/3594865
# file_link_display
